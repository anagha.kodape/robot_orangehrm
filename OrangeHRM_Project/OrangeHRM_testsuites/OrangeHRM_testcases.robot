*** Settings ***
Library    SeleniumLibrary
Resource    ../OrangeHRM_config/OrangeHRM_locators.robot
Resource    ../OrangeHRM_utilities/OrangeHRM_keywords.robot

*** Test Cases ***

LoginTest
    Open my Browser
    Sleep    3 seconds
    Enter Username
    Enter Password
    Click Login button
    Sleep    5 seconds
    
MyInfoTest 
    Click on MyInfo tab
    Sleep    3 seconds    
    Close my Browser

   