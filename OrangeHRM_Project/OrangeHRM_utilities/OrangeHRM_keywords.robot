*** Settings ***
Library    SeleniumLibrary
Resource    ../OrangeHRM_config/OrangeHRM_locators.robot

*** Keywords ***
Open my Browser
    Open Browser    ${url}    ${browser}
    Maximize Browser Window
    
Close my Browser
    Close All Browsers
    
Enter Username
    Input Text    ${username}    ${txt_username}    

Enter Password
    Input Text    ${password}    ${txt_password}    
    
Click Login button
    Click Element    ${login_btn}    
    
# Keywords for My Info page

Click on MyInfo tab
    Click Element    ${myinfo} 
    