*** Variables ***

# URL Variables
${browser}    chrome
${url}    https://opensource-demo.orangehrmlive.com/web/index.php/auth/login

# Login Variables
${txt_username}    Admin
${txt_password}    admin123


############## Locators ##############

# Login Locators
${username}    xpath://input[@placeholder='Username']
${password}    xpath://input[@placeholder='Password']
${login_btn}    xpath://button[normalize-space()='Login']

# My Info  Locators

${myinfo}    xpath://span[normalize-space()='My Info']
${employeefullname}    xpath://input[@placeholder='First Name']    
${employeemiddlename}    xpath://input[@placeholder='First Name']
${employeelastname}    xpath://input[@placeholder='Last Name']







#